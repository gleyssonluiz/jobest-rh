const csurf = require('csurf')
const morgan = require('morgan')
const helmet = require('helmet')
const consign = require('consign')
const express = require('express')
const cookieParser = require('cookie-parser')
const validatorExpress = require('express-validator')
const cors = require('./app/v1/middlewares/http')

class App {
    constructor(){
        this.app = express()
        this.middlewares()
        this.load()
    }

    middlewares(){
        this.app.use(helmet())
        this.app.use(cookieParser())
        this.app.use(morgan('dev'))
        this.app.use(express.json())        
        this.app.use(validatorExpress())
        this.app.use(express.urlencoded({extended:false})) 
        this.app.use(cors)
    }

    load(){
        consign()
        .include('src/database/relations')
        .include('src/app/v1/controllers')
        .then('src/app/v1/routes')
        .into(this.app)
    }
}

module.exports = new App().app
