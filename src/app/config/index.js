module.exports = {
    pagination: {
        page: 1,
        perpage: 25
    },

    hashConfig: {
        salt: 12
    },

    user: {
        levels: ['PRESIDENTE', 'DIRECTION', 'RH', 'ADM', 'RECRUTADOR', 'AREA', 'CONTROLADORIA']
    }
}