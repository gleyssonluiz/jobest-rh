const { encrypt } = require('./utils')

module.exports = {

    getUser: async (body) => {
        const single = {}
        single.nome = body.name || null
        single.email = body.email || null
        single.senha = await encrypt(body.password) || null
        single.customer_id = body.id_client || null
        single.nivel = body.level || null
        return single
    },

    amountCompany: async (body) => {
        const single = {}
        single.customer_id = body.id_client || null
        single.razao_social = body.name || null
        single.cnpj = body.cnpj || null
        return single
    },

    amountDirection: async (body) => {
        const single = {}
        single.customer_id = body.id_client || null
        single.responsavel_id = body.id_responsible || null
        single.nome = body.name || null
        return single
    },

    amountDepartment: async (body) => {
        const single = {}
        single.customer_id = body.id_client || null
        single.direction_id = body.id_direction || null
        single.responsavel_id = body.id_responsible || null
        single.nome = body.name || null
        return single
    },

    amountcostCenter: async (body) => {
        const single = {}
        single.customer_id = body.id_client || null
        single.direction_id = body.id_direction || null
        single.responsavel_id = body.id_responsible || null
        single.nome = body.name || null
        return single
    }

}