const bcrypt = require ('bcrypt')
const { hashConfig } = require('../config')
const jwt = require('jsonwebtoken');


module.exports = {

    encrypt: async function(password){
        try {
            return await bcrypt.hash(password, hashConfig.salt)
        } catch (error) {
            throw 'Error hash password'
        }
    },

    generateJWT: async function(user){
        return await jwt.sign({
            id: user.id,
            id_client: user.id_customer,
            name: user.nome
        }, process.env.SECRET_KEY, {
            expiresIn: 36000
        })
    },

    verifyJWT: async function(token){
        let decoded = await jwt.verify(token , process.env.SECRET_KEY)
        console.log(decoded)
    }
}