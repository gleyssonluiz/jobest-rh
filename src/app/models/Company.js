const sequelizePaginate = require('sequelize-paginate')

class Company {
    initModule(sequelize, DataTypes){
        const model = sequelize.define('Company', {
            id: { 
                type: DataTypes.UUID ,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true,
                allowNull: false
            },

            CustomerId: {
                type: DataTypes.UUID,
                field: 'customer_id',
                allowNull: false
            },
            
            name: DataTypes.STRING,
            cnpj: DataTypes.STRING
        });

        sequelizePaginate.paginate(model)
        return model
    }
}

module.exports = new Company().initModule

