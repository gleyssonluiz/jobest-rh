const sequelizePaginate = require('sequelize-paginate')

class CostCenter {
    initModule(sequelize, DataTypes){
        const model = sequelize.define('CostCenter', {
            id: {
                type: DataTypes.UUID ,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true,
                allowNull: false
            },

            CustomerId: {
                type: DataTypes.UUID,
                field: 'customer_id'
            },

            name: {
                type: DataTypes.STRING,
                allowNull: false,
            }
        });

        sequelizePaginate.paginate(model)
        return model

    }
}

module.exports = new CostCenter().initModule

