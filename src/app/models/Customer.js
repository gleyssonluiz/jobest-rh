const sequelizePaginate = require('sequelize-paginate')

class Usuario {
    initModule(sequelize, DataTypes){
        const model = sequelize.define('Customer', {
            id: { 
                type: DataTypes.UUID ,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true,
                allowNull: false
            },
            name: {
               type: DataTypes.STRING,
               allowNull: false
            },

            email: {
                type: DataTypes.STRING,
                allowNull: false,
            },

            numberDoc: {
                type: DataTypes.STRING,
                allowNull: false,
            },

            typeDoc: {
                type: DataTypes.ENUM,
                values: ['cpf', 'cnpj'],
                allowNull: false,
            }

        });

        sequelizePaginate.paginate(model)
        return model

    }
}

module.exports = new Usuario().initModule

