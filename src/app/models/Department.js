const sequelizePaginate = require('sequelize-paginate')

class Department {
    initModule(sequelize, DataTypes){
        const model = sequelize.define('Department', {
            id: { 
                type: DataTypes.UUID ,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true,
                allowNull: false
            },
            CustomerId: {
                type: DataTypes.UUID,
                field: 'customer_id'
            },

            name: DataTypes.STRING
        });

        sequelizePaginate.paginate(model)
        return model
    }
}

module.exports = new Department().initModule

