const sequelizePaginate = require('sequelize-paginate')

class Direction {
    initModule(sequelize, DataTypes){
        const model = sequelize.define('Direction', {
            id: { 
                type: DataTypes.UUID ,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true,
                allowNull: false
            },

            CustomerId: {
                type: DataTypes.UUID,
                field: 'customer_id'
            },
          
            name: DataTypes.STRING
        });

        sequelizePaginate.paginate(model)
        return model
    }
}

module.exports = new Direction().initModule

