const sequelizePaginate = require('sequelize-paginate')

class User {
    initModule(sequelize, DataTypes){
        const model = sequelize.define('User', {
            id: { 
                type: DataTypes.UUID ,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true,
                allowNull: false
            },

            CustomerId: {
                type: DataTypes.UUID,
                field: 'customer_id'
            },

            name: DataTypes.STRING,
            email: DataTypes.STRING,
            password: DataTypes.STRING,
            level: {
                type: DataTypes.ENUM,
                values: ['PRESIDENTE', 'DIRETORIA', 'RH', 'ADM', 'RECRUTADOR', 'AREA', 'CONTROLADORIA']
            },
        });

        sequelizePaginate.paginate(model)
        return model
    }
}

module.exports = new User().initModule

