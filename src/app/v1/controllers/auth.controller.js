const { User } =  require('../../models')
const { matchedData } = require('express-validator/filter')
const { generateJWT } = require('../../libs/utils')
const bcrypt = require('bcrypt')


module.exports = function(app){

    const post = async (req,res) => {

        try{
          const body = matchedData(req, { locations: ['body'] });
          const data = await User.findOne({
              where: {
                email: body.email
              }
          })

          if(!data) return res.status(401).send({})

          if( await bcrypt.compare(body.password, data.password) ){
            return res.send({
              token: await generateJWT(data)
            })
          }

          return res.status(401).send({})
        } catch (error) {
          console.log(error)
            return res.status(500).send(error)
        }
    }
  
    app.controllers = app.controllers || {}
    app.controllers.auth = { post }
  }