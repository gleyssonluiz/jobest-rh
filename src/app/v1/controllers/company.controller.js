const { Company } =  require('../../models')
const { matchedData } = require('express-validator/filter')
const { pagination } = require('../../config')


module.exports = function(app){

    const get = async (req,res) => {
      try{

          const query = matchedData(req, { locations: ['query'] });
          const data = await Company.paginate({
              page: parseInt(query.page) || pagination.page,
              paginate: parseInt(query.perpage) || pagination.perpage,
              order: [['name','DESC']],
              where: {
                customer_id: req.customer_id
              }
          })

          return res.send(data)
      } catch (error) {
          console.log(error)
          res.status(500).send(error)
      }
  }

  const post = async (req,res) => {
      try{

          const body = matchedData(req, { locations: ['body'] });
          body.CustomerId = req.customer_id 
          const data = await Company.create(body)

          return res.send(data)
      } catch (error) {
          console.log(error)
          return res.status(500).send(error)
      }
  }

  const update = async (req,res) => {
    try{
        const body = matchedData(req, { locations: ['body'] });

        const data = await Company.update(body,{
          where:{
            customer_id: req.customer_id,
            id: req.params.id
          }
        })

        return res.send(data)
    } catch (error) {
        console.log(error)
        return res.status(500).send(error)
    }
  }

  const remove = (req,res) => {
    return res.send([
      { name:"Grupo Opovo" , cnpj: "111.111.11/25-565" }
    ])

  }

  app.controllers = app.controllers || {}
  app.controllers.company = { get, post, update, remove }
}