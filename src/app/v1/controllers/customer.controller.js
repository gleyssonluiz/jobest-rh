const { Customer } =  require('../../models')
const { matchedData } = require('express-validator/filter')
const { pagination } = require('../../config')

module.exports = function(app){

    const get = async (req,res) => {
        try{
            const query = matchedData(req, { locations: ['query'] });
            const data = await Customer.paginate({
                page: parseInt(query.page) || pagination.page,
                paginate: parseInt(query.perpage) || pagination.perpage,
                order: [['name','ASC']]
            })

            return res.send(data)
        } catch (error) {
            res.status(500).send(error)
        }
    }
  
    const post = async (req,res) => {
        try{
            const body = matchedData(req, { locations: ['body'] });
            const data = await Customer.create(body)
            return res.send(data)
        } catch (error) {
            return res.status(500).send(error)
        }
    }
  
    const update = async (req,res) => {
      try{
          console.log("update")
          const body = matchedData(req, { locations: ['body'] });
          const data = await Customer.update(body, {
            where: {
              id: req.params.id
            }
          })
          
          return res.send({})
      } catch (error) {
          return res.status(500).send(error)
      }
    }
  
    const remove = (req,res) => {
      return res.send([
        { name:"Grupo Opovo" , cnpj: "111.111.11/25-565" }
      ])
    }
  
    app.controllers = app.controllers || {}
    app.controllers.customer = { get, post, update, remove }
  }