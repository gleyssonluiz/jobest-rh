const { Department, Direction, User } =  require('../../models')
const { matchedData } = require('express-validator/filter')
const { amountDepartment } = require('../../libs/models')
const { pagination } = require('../../config')


module.exports = function(app){

  let _include = [
    {
      model: Direction,
      as: 'direction',
      attributes: ['id', 'name']
    },
    {
      model: User,
      as: 'responsible',
      attributes: ['name', 'email', 'level']
    }
  ]

    const get = async (req,res) => {
      try{
          const query = matchedData(req, { locations: ['query'] });
          const data = await Department.paginate({
              attributes: ['id','name','createdAt','updatedAt'],
              page: parseInt(query.page) || pagination.page,
              paginate: parseInt(query.perpage) || pagination.perpage,
              order: [['name','ASC']],
              include: _include,
              where: {
                customer_id: req.customer_id || null
              }
          })

          return res.send(data)
      } catch (error) {
          console.log(error)
          res.status(500).send(error)
      }
  }

  const post = async (req,res) => {
      try{
          const body = matchedData(req, { locations: ['body'] });
          body.CustomerId = req.customer_id
          const data = await Department.create(body)

          return res.send(data)
      } catch (error) {
          console.log(error)
          return res.status(500).send(error)
      }
  }

  const update = async (req,res) => {
    try{
        const body = matchedData(req, { locations: ['body'] });

        const data = await Department.update(body,{
          where:{
            customer_id: req.customer_id,
            id: req.params.id
          }
        })

        return res.send({})
    } catch (error) {
        console.log(error)
        return res.status(500).send(error)
    }
  }

  const remove = (req,res) => {
    return res.send([
      { name:"Grupo Opovo" , cnpj: "111.111.11/25-565" }
    ])

  }

  app.controllers = app.controllers || {}
  app.controllers.department = { get, post, update, remove }
}