const { User, Customer } =  require('../../models')
const { encrypt } = require('../../libs/utils')
const { matchedData } = require('express-validator/filter')

module.exports = function(app){

    let _include = [
        {
            model: Customer,
            attributes: ['name', 'id']
        }
    ]

    const post = async (req,res) => {
        try{
            const body = matchedData(req, { locations: ['body'] });
            body.password = await encrypt(body.password)
            const data = await User.create(body)
            return res.send(data)

        } catch (error) {
            return res.status(500).send(error)
        }
    }

    const postCustomer = async (req,res) => {
        try{
            const body = matchedData(req, { locations: ['body'] });
            const data = await Customer.create(body)
            return res.send(data)
        } catch (error) {
            return res.status(500).send(error)
        }
    }
  
  
    const get = async (req,res) => {
        try{
            const data = await User.findAll({
                include: _include,
                attributes: ['id','name','email','level']
            })
            return res.send(data)
        } catch (error) {
            console.log(error)
            return res.status(500).send(error)
        }
    }

    const update = (req,res) => {
       return res.send({})
    }
  
    const remove = (req,res) => {
       return res.send({})
  
    }
  
    app.controllers = app.controllers || {}
    app.controllers.local = { get, post, update, remove, postCustomer }
    
  }