const { User } =  require('../../models')
const { matchedData } = require('express-validator/filter')
const { encrypt } = require('../../libs/utils')
const { pagination } = require('../../config')

module.exports = function(app){

    const get = async (req,res) => {
        try{
            const query = matchedData(req, { locations: ['query'] });
            const data = await User.paginate({
                page: parseInt(query.page) || pagination.page,
                paginate: parseInt(query.perpage) || pagination.perpage,
                order: [['name','DESC']],
                where: {
                  customer_id: req.customer_id || null
                }
            })

            return res.send(data)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }
  
    const post = async (req,res) => {
        try{
            const body = matchedData(req, { locations: ['body'] });
            body.CustomerId = req.customer_id
            body.password = await encrypt(body.password)
            const data = await User.create(body)
            return res.send({id: data.id, name: data.name, email: data.email})
        } catch (error) {
            console.log(error)
            return res.status(500).send(error)
        }
    }
  
    const update = async (req,res) => {
        try{
            const body = matchedData(req, { locations: ['body'] });
            body.password = await encrypt(body.password)
            const data = await User.update(body,{
                where:{
                    id: req.params.id,
                    CustomerId: req.customer_id
                }
            })
            return res.send({id: data.id, name: data.name, email: data.email})
        } catch (error) {
            console.log(error)
            return res.status(500).send(error)
        }
    }
  
    const remove = (req,res) => {
      return res.send({})
    }
  
    app.controllers = app.controllers || {}
    app.controllers.user = { get, post, update, remove }
  }