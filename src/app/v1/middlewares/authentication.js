const { User } = require('../../models')
const { generateJWT } = require('../../libs/utils')
const jwt = require('jsonwebtoken')

module.exports = {

    isAuthenticate: async (req, res, next) =>{

        try{
            const token = req.headers['authorization'];
            const type = token.split(' ')[0]
            const payload = token.split(' ')[1]

            if (!token) return res.status(401).send({ error: 'No token provided.' });
            if ( type !== 'Bearer') return res.status(401).send({error: 'Token bad formated'})
    
            const data = await jwt.verify(payload, process.env.SECRET_KEY)
            const data_user = await User.findOne({
                attributes: ['customer_id'],
                where: {
                  id: data.id
                }
              })
    
            if(!data_user) return res.status(401).send()
            
            req.user_id = data.id
            req.customer_id = data_user.dataValues.customer_id
            next();
            
        }catch(error){
            console.log(error)
            return res.status(401).send({})
        }
       
    },



    refreshAuth: async (req, res, next) =>{

        try{
           let token = await generateJWT({
               id: req.id_user,
               id_client: req.id_client,
               name: req.username
           })
            res.status(200).send({ token })
        }catch(error){
            return res.status(401).send({})
        }
       
    }
}