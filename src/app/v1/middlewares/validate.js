const { validationResult } = require('express-validator/check');

module.exports = {

    validate: (req, res, next) => {
        const erros = validationResult(req)
        
        if(erros.isEmpty()){
            return next()
        }

        return res.status(422).send({
            error: erros.array()
        })
    }
}