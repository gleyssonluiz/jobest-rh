module.exports = function(app) {
    app.get('/api', (req, res, next) => {
        res.status(200).send({
            message: "Service is Running!"
        })
    })
}