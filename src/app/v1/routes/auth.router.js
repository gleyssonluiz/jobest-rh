const { body } = require('express-validator/check')
const { validate } = require('../middlewares/validate')
const { isAuthenticate, refreshAuth } = require('../middlewares/authentication')

module.exports = function(app) {

    app.get('/api/user/authenticate', 
        isAuthenticate,
        refreshAuth
    ) /** FIM */

    app.post('/api/user/authenticate',
    [
        body('email')
        .isString()
        .withMessage("O email deve ser texto")
        .isEmail()
        .withMessage("Informe um endereço de email válido"),
        body('password')
        .isString()
        .withMessage("A senha deve ser em formato de texto"),
    ],
        validate,
        app.controllers.auth.post
    ) /** FIM */
}