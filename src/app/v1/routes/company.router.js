const { body, query } = require('express-validator/check')
const { isAuthenticate } = require("../middlewares/authentication")
const { validate } = require('../middlewares/validate')

module.exports = function(app) {

    app.get('/api/v1/company',[
        query('page')
        .optional()
        .isInt()
        .withMessage("Page deve ser um número inteiro"),
        query('perpage')
        .optional()
        .isInt()
        .withMessage("Perpage deve ser um número inteiro")
    ],        
        validate,
        isAuthenticate,
        app.controllers.company.get

    ) /** FIM */


    app.post('/api/v1/company',
    [
        body('name')
        .isString()
        .withMessage("O nome deve ser em formato de texto"),
        body('cnpj')
        .isString()
        .withMessage("O CNPJ deve ser em formato de texto")
    ],
        validate,
        isAuthenticate,
        app.controllers.company.post

    ) /** FIM */


    app.put('/api/v1/company/:id',
    [
        body('name')
        .isString()
        .withMessage("O nome deve ser em formato de texto"),
        body('cnpj')
        .isString()
        .withMessage("O CNPJ deve ser em formato de texto")
    ],
        validate,
        isAuthenticate,
        app.controllers.company.update

    ) /** FIM */

}