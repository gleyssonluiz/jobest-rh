const { body, query } = require('express-validator/check')
const { isAuthenticate } = require("../middlewares/authentication")
const { validate } = require('../middlewares/validate')

module.exports = function(app) {

    app.get('/api/v1/cost-center',[
        query('page')
        .optional()
        .isInt()
        .withMessage("Page deve ser um número inteiro"),
        query('perpage')
        .optional()
        .isInt()
        .withMessage("Perpage deve ser um número inteiro")
    ],        
        validate,
        isAuthenticate,
        app.controllers.centers.get

    ) /** FIM */


    app.post('/api/v1/cost-center',
    [
        body('name')
        .isString()
        .withMessage("O nome deve ser em formato de texto"),
        body('responsible_id')
        .isString()
        .withMessage("O ID deve ser formato de texto"),
        body('direction_id')
        .isString()
        .withMessage("O ID deve ser formato de texto")
    ],
        validate,
        isAuthenticate,
        app.controllers.centers.post

    ) /** FIM */


    // app.put('/api/user', app.controllers.user.update)
    // app.delete('/api/user', app.controllers.user.post)
}