const { body, query } = require('express-validator/check')
const { validate } = require('../middlewares/validate')
const { isAuthenticate } = require('../middlewares/authentication')


module.exports = function(app) {

    app.get('/api/v1/customer',[
        query('page')
        .optional()
        .isInt()
        .withMessage("Page deve ser um número inteiro"),
        query('perpage')
        .optional()
        .isInt()
        .withMessage("Perpage deve ser um número inteiro")
    ],        
        validate, 
        isAuthenticate,
        app.controllers.customer.get

    ) /** FIM */


    app.post('/api/v1/customer',
    [
        body('name')
        .isString()
        .withMessage("O nome deve ser texto"),
        body('email')
        .isString()
        .withMessage("O email deve ser texto")
        .isEmail()
        .withMessage("Informe um endereço de email válido"),
        body('numberDoc')
        .isString()
        .withMessage("O número do documento deve ser texto"),
        body('typeDoc')
        .isString()
        .withMessage("O número do documento deve ser cpf ou cnpj"),
    ],
        validate,
        isAuthenticate,
        app.controllers.customer.post
    ) /** FIM */


    app.put('/api/v1/customer/:id',
    [
        body('name')
        .optional()
        .isString()
        .withMessage("O nome deve ser texto"),
        body('email')
        .optional()
        .isString()
        .withMessage("O email deve ser texto")
        .isEmail()
        .withMessage("Informe um endereço de email válido"),
        body('numberDoc')
        .optional()
        .isString()
        .withMessage("O número do documento deve ser texto"),
        body('typeDoc')
        .optional()
        .isString()
        .withMessage("O número do documento deve ser cpf ou cnpj"),
    ],
        validate,
        isAuthenticate,
        app.controllers.customer.update
    ) /** FIM */

}