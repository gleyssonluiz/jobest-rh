const { body, query } = require('express-validator/check')
const { isAuthenticate } = require("../middlewares/authentication")
const { validate } = require('../middlewares/validate')

module.exports = function(app) {

    app.get('/api/v1/department',[
        query('page')
        .optional()
        .isInt()
        .withMessage("Page deve ser um número inteiro"),
        query('perpage')
        .optional()
        .isInt()
        .withMessage("Perpage deve ser um número inteiro")
    ],        
        validate,
        isAuthenticate,
        app.controllers.department.get

    ) /** FIM */


    app.post('/api/v1/department',
    [
        body('name')
        .isString()
        .withMessage("O nome deve ser em formato de texto"),
        body('responsible_id')
        .isString()
        .withMessage("O ID deve ser formato de texto"),
        body('direction_id')
        .isString()
        .withMessage("O ID deve ser formato de texto")
    ],
        validate,
        isAuthenticate,
        app.controllers.department.post

    ) /** FIM */


    app.put('/api/v1/department/:id',
    [
        body('name')
        .isString()
        .withMessage("O nome deve ser em formato de texto"),
        body('responsible_id')
        .isString()
        .withMessage("O ID deve ser formato de texto"),
        body('direction_id')
        .isString()
        .withMessage("O ID deve ser formato de texto")
    ],
        validate,
        isAuthenticate,
        app.controllers.department.update

    ) /** FIM */



    // app.put('/api/user', app.controllers.user.update)
    // app.delete('/api/user', app.controllers.user.post)
}