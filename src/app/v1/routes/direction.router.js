const { body, query } = require('express-validator/check')
const { isAuthenticate } = require("../middlewares/authentication")
const { validate } = require('../middlewares/validate')

module.exports = function(app) {

    app.get('/api/v1/direction',[
        query('page')
        .optional()
        .isInt()
        .withMessage("Page deve ser um número inteiro"),
        query('perpage')
        .optional()
        .isInt()
        .withMessage("Perpage deve ser um número inteiro")
    ],        
        validate,
        isAuthenticate,
        app.controllers.direction.get

    ) /** FIM */

    app.post('/api/v1/direction',
    [
        body('responsible_id')
        .optional()
        .isString()
        .withMessage("O ID deve ser formato de texto"),
        body('name')
        .optional()
        .isString()
        .withMessage("O nome deve ser em formato de texto")
    ],
        validate,
        isAuthenticate,
        app.controllers.direction.post

    ) /** FIM */

    app.put('/api/v1/direction/:id',
    [
        body('responsible_id')
        .isString()
        .withMessage("O ID deve ser formato de texto"),
        body('name')
        .isString()
        .withMessage("O nome deve ser em formato de texto")
    ],
        validate,
        isAuthenticate,
        app.controllers.direction.update

    ) /** FIM */


    // app.put('/api/user', app.controllers.user.update)
    // app.delete('/api/user', app.controllers.user.post)
}