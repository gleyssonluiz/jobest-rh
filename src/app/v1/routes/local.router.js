const { body, query } = require('express-validator/check')
const { validate } = require('../middlewares/validate')

module.exports = function(app) {

    if(process.env.NODE_ENV === 'development'){

        app.get('/local/user',app.controllers.local.get )

        app.post('/local/user',
        [
            body('CustomerId')
            .isString()
            .withMessage("O id deve ser texto"),
            body('name')
            .isString()
            .withMessage("O nome deve ser texto"),
            body('email')
            .isString()
            .withMessage("O email deve ser em formato de texto")
            .isEmail()
            .withMessage("Informe um endereço de email válido"),
            body('password')
            .isString()
            .withMessage("O password deve ser em formato de texto"),
            body('level'),
            body('customer_id')
     
        ],
            validate,
            app.controllers.local.post

        ) /** FIM */

        app.post('/local/customer',
        [
     
            body('name')
            .isString()
            .withMessage("O nome deve ser texto"),
            body('email')
            .isString()
            .withMessage("O email deve ser em formato de texto")
            .isEmail()
            .withMessage("Informe um endereço de email válido"),
            body('numberDoc')
            .isString()
            .withMessage("O número do documento deve ser em formato de texto"),
            body('typeDoc')
            .isString()
            .withMessage("O tipo do documento deve ser em formato de texto")
     
        ],
            validate,
            app.controllers.local.postCustomer

        ) /** FIM */

    }
    


    // app.put('/api/user', app.controllers.user.update)
    // app.delete('/api/user', app.controllers.user.post)
}