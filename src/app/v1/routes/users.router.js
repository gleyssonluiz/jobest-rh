const { body, query } = require('express-validator/check')
const { isAuthenticate } = require("../middlewares/authentication")
const { validate } = require('../middlewares/validate')

module.exports = function(app) {

    app.get('/api/v1/user/',[
        query('page')
        .optional()
        .isInt()
        .withMessage("Page deve ser um número inteiro"),
        query('perpage')
        .optional()
        .isInt()
        .withMessage("Perpage deve ser um número inteiro")
    ],        
        validate, 
        isAuthenticate,
        app.controllers.user.get
    ) /** FIM */

    app.post('/api/v1/user/',
    [
        body('name')
        .isString()
        .withMessage("O nome deve ser texto"),
        body('email')
        .isString()
        .withMessage("O email deve ser em formato de texto")
        .isEmail()
        .withMessage("Informe um endereço de email válido"),
        body('password')
        .isString()
        .withMessage("O password deve ser em formato de texto"),
        body('level')
    ],
        validate,
        isAuthenticate,
        app.controllers.user.post

    ) /** FIM */


    app.put('/api/v1/user/:id',
    [
        body('name')
        .isString()
        .withMessage("O nome deve ser texto"),
        body('email')
        .isString()
        .withMessage("O email deve ser em formato de texto")
        .isEmail()
        .withMessage("Informe um endereço de email válido"),
        body('password')
        .isString()
        .withMessage("O password deve ser em formato de texto"),
        body('level')
    ],
        validate,
        isAuthenticate,
        app.controllers.user.update

    ) /** FIM */


    // app.put('/api/user', app.controllers.user.update)
    // app.delete('/api/user', app.controllers.user.post)
}