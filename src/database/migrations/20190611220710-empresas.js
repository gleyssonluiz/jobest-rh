'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('companys', { 
      
      id: {
        type: Sequelize.INTEGER ,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },

      id_customer: {
        type: Sequelize.INTEGER,
        references: { model: 'customers', key: 'id' },
        allowNull: false,
      },

      razao_social: {
        type: Sequelize.STRING,
      },

      cnpj: {
        type: Sequelize.STRING,
      },

      created_at:{
        type: Sequelize.DATE,
        allowNull: false
      },

      updated_at:{
        type: Sequelize.DATE,
        allowNull: false
      }
      
    });
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.dropTable('companys');
  }
};
