'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('departmentes', { 
      id: {
        type: Sequelize.INTEGER ,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      
      id_customer: {
        type: Sequelize.INTEGER ,
        references: { model: 'customers', key: 'id' },
        allowNull: false
      },

      id_responsavel: {
        type: Sequelize.INTEGER ,
        allowNull: false
      },

      id_direction: {
        type: Sequelize.INTEGER ,
        allowNull: false
      },

      nome: {
        type: Sequelize.STRING,
        allowNull: false,
      },

      created_at:{
        type: Sequelize.DATE,
        allowNull: false
      },

      updated_at:{
        type: Sequelize.DATE,
        allowNull: false
      }
      
    });
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.dropTable('departmentes');
  }
};
