'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('cargos', { 
      id: {
        type: Sequelize.INTEGER ,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },

      id_customer: {
        type: Sequelize.INTEGER ,
        references: { model: 'customers', key: 'id' },
        allowNull: false
      },

      id_direction: {
        type: Sequelize.INTEGER ,
        allowNull: false
      },

      id_department: {
        type: Sequelize.INTEGER ,
        allowNull: false
      },

      cbo: {
        type: Sequelize.STRING,
        allowNull: false,
      },

      descricao: {
        type: Sequelize.STRING,
        allowNull: false,
      },

      id_nivel_cargo: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },

      flg_experiencia_area: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },

      flg_experiencia_funcao: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },

      habilidades_tecnicas: {
        type: Sequelize.TEXT,
        allowNull: false,
      },

      responsabilidades: {
        type: Sequelize.TEXT,
        allowNull: false,
      },

      indicadores: {
        type: Sequelize.TEXT,
        allowNull: false,
      },

      created_at:{
        type: Sequelize.DATE,
        allowNull: false
      },

      updated_at:{
        type: Sequelize.DATE,
        allowNull: false
      }
      
    });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
