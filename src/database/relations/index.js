const db = require("../../app/models")
const { CostCenter, Department, Customer, User, Company, Direction } = db

module.exports = function(app) {
    
    Customer.hasMany(CostCenter)
    Customer.hasMany(Direction)
    Customer.hasMany(Department)
    Customer.hasMany(User)
    Customer.hasMany(Company)

    User.belongsTo(Customer)

    CostCenter.belongsTo(Customer)
    CostCenter.belongsTo(Direction, { as:'direction', foreignKey: 'direction_id'})
    CostCenter.belongsTo(User, { as:'responsible', foreignKey: 'responsible_id'})

    Department.belongsTo(Customer)
    Department.belongsTo(Direction, { as:'direction', foreignKey:'direction_id'})
    Department.belongsTo(User, { as:'responsible', foreignKey:'responsible_id'})

    Direction.belongsTo(Customer)
    Direction.belongsTo(User, { as:'responsible', foreignKey: 'responsible_id'})

    Direction.hasMany(CostCenter, { foreignKey: 'direction_id'})
    Direction.hasMany(Department, { as: 'direction', foreignKey: 'direction_id'})

    Company.belongsTo(Customer)

    // db.sequelize.sync({force: true})
}

