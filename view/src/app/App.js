import React from 'react';
import Theme from './components/theme/theme'
import Dashboard from './pages/dashboard/'
import User from './pages/user/'
import api from './api/authenticate'
import './global.css'

import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

function App() {
 
    const isAuth = () => {
      
    }
    

    return (
        <Router>
            <Switch>
                <Route path="/dashboard">
                    <Theme content={Dashboard}/>
                </Route>
                <Route exact path="/">
                    <User/>
                </Route>
            </Switch>
        </Router>
    );
}

export default App;
