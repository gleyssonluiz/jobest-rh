import storage from '../storage/'
import axios from 'axios'

const api = axios.create({baseURL: 'http://localhost:5000/api/', timeout: 20000});

const auth = async (data) => {
    let response = await api.post("/user/authenticate", data)
    storage.setToken(response.data.token)
}

const isAuth = async (data) => {
    let response = await api.get("/user/logged/", data)
    storage.setToken(response.data.token)
}

const logout = (success, error) => {
    storage.removeToken()
}

export default {auth, logout, isAuth}
