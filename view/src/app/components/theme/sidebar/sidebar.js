import React, { Fragment } from 'react';
import { MdChevronRight, MdPerson } from 'react-icons/md'
import Nav from './style'

const Sidebar = function(props){

    return(
        <Nav marginLeft={props.show} >
            <div className="sidebar-header">
                <div className="header">
                    <h1>JOBEST RS</h1>
                </div>
                <div className="profile">
                    <div className="photo">  </div>
                   <div> <h2 className="name" > Gleysson Rocha </h2>  <small> Diretor de Tecnologia </small>  </div> 
                </div>
            </div>
            <ul className="list-unstyled components">
                <li className="active">
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" className="dropdown-toggle"><MdChevronRight size="22"/> Corporativos</a>
                    <ul className="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="#"><MdChevronRight size="22"/> Companys</a>
                        </li>
                        <li>
                            <a href="#"><MdChevronRight size="22"/> Departmentes</a>
                        </li>
                        <li>
                            <a href="#"><MdChevronRight size="22"/> costCenters de Custo</a>
                        </li>
                        <li>
                            <a href="#"><MdChevronRight size="22"/> Directions</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><MdChevronRight size="22"/> Candidatos</a>
                </li>
                <li>
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" className="dropdown-toggle"><MdChevronRight size="22"/> Vagas</a>
                    <ul className="collapse list-unstyled" id="pageSubmenu">
                        <li>
                            <a href="#"><MdChevronRight size="22"/> Nova Vaga</a>
                        </li>
                        <li>
                            <a href="#"><MdChevronRight size="22"/> Validações de Vaga</a>
                        </li>
                        <li>
                            <a href="#"><MdChevronRight size="22"/> Andamento da Seleção</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><MdChevronRight size="22"/> Configurações</a>
                </li>
         
            </ul>
           
        </Nav>
    )
}

export default Sidebar