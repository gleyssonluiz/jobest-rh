import React, { Fragment } from 'react';
import styled from 'styled-components'

const Nav = styled.nav`

    width: 250px;
    position: fixed;
    top: 0;
    left: 0;
    height: 100vh;
    z-index: 999;
    background: var(--color-main);
    color: #fff;
    transition: all 0.3s;
    margin-left:${ (props) => props.marginLeft ? 0: `${-240}px` };
    box-shadow: 3px 70px 5px var(--color-shadow);
    overflow-y: auto;


    scrollbar-color:  var(--color-main-ligth) transparent;
    scrollbar-width: thin;


:hover{
    margin-left: 0;
}

.header h1{
    color:#EEE;
    font-size:20px;
    padding-bottom:5px;
    font-weight:700;
}


.sidebar-header {
    padding:30px 25px;
    margin-bottom:20px;
    background: var(--color-main-dark);
}

.profile{
   
    display: -ms-flexbox;
	display: -webkit-flex;
	display: flex;

	-ms-flex-align: center;
	-webkit-align-items: center;
	-webkit-box-align: center;

	align-items: center;
    justify-content:space-between;
}

.profile .photo{
    min-width:50px;
    min-height:50px;
    border-radius:50%;
    background:#FFF;
}

.profile .name{
    font-size:15px;
    max-width:180px;
    font-weight:700;
    padding:0;
    margin:0;
    line-height:1em;
}
.profile .name small{
    font-size:12px;
    font-style:bold;
    width:100%;
}


ul.components {
    padding: 25px 0;
}

ul p {
    color: #fff;
    padding: 10px;
}


ul li a {
    padding: 10px 25px;
    display:flex;
    vertical-align: middle;
}

ul li a:hover {
    color: var(--color-main-ligth);
    background: #fff;
}

ul li.active>a,
a[aria-expanded='true'] {
    color: #fff;
    background: var(--color-main-dark);
}

a[data-toggle="collapse"] {
    position: relative;
}

.dropdown-toggle::after {
    display: block;
    position: absolute;
    top: 50%;
    right: 20px;
    transform: translateY(-50%);
}

ul ul a {
    font-size: 15px !important;
    padding-left: 40px !important;
    background: var(--color-main-ligth);
}

`
export default Nav 
