import React, { Fragment } from 'react';
import styled from 'styled-components'

const Container = styled.nav`
    width: ${(props) => props.show ? 'calc(100% - 250px)' : '100%'  };
    padding:0;
    min-height: 100vh;
    transition: all 0.3s;
    position: absolute;
    top: 0;
    right: 0;
`

const Wraper =  styled.div`
    display: flex;
    width: 100%;
`

const Content = styled.div`
    margin: 25px;
    padding:20px 40px;
    background:#FFF;
    border-radius:5px;
    min-height:78vh;
`

export { Container, Wraper, Content }
