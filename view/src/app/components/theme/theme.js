import React, { useState } from 'react'
import Sidebar from './sidebar/sidebar'
import Navigation from './navigation/navigation'

import { Wraper, Container, Content  }  from './style.js'

const Theme = function (props) {

    const [show, setShow] = useState(true);

    return (

        <Wraper>
            <Sidebar show={show} />
            <Container id="content" show={show} >
                <Navigation toogleSideBar={() => setShow(!show)}  />
                <Content> 
                    <props.content></props.content>
                </Content>
            </Container>
        </Wraper>

    )
}

export default Theme