import React, {useState} from 'react'
import {Redirect} from 'react-router-dom'
import FormUser from './style'
import api from '../../api/authenticate'

const User = function (props) {

    const [load, setLoad] = useState(false)
    const [error, setError] = useState(false)
    const [user, setUser] = useState({email: 'gleysson.luiz@opovodigital.com', password: 'm@bel'})

    const handleEmail = (e) => {
        setUser({
            ...user,
            email: e.target.value
        })
    }

    const handleSenha = (e) => {
        setUser({
            ...user,
            password: e.target.value
        })
    }

    const handleError = (err) => {
        if (err) {
            return (
                <div className="message msg-error">
                    Verifique suas credenciais e tente novamente.</div>
            )
        }
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            await api.auth(user)
            setLoad(true)
        } catch (err) {
            setError(true)
        }
    }

    if (load) {
        return (
            <Redirect to="/dashboard"/>
        )
    }

    return (
        <FormUser>
            <section>
                <div className="container">
                    <div className="row">
                        <div className="offset-md-4 col-md-4">
                            <div className="box">
                                <form onSubmit={handleSubmit}>
                                    <h1>JobestApp</h1>
                                    <div className="form-group">
                                        <label>
                                            Email:
                                        </label>
                                        <input type="email"
                                            value={
                                                user.email
                                            }
                                            onChange={handleEmail}
                                            placeholder="seuemail@company.com.br"
                                            required
                                            className="form-control"/>
                                    </div>

                                    <div className="form-group">
                                        <label>
                                            Senha:
                                        </label>
                                        <input type="password"
                                            value={
                                                user.password
                                            }
                                            onChange={handleSenha}
                                            placeholder="**********"
                                            required
                                            className="form-control"/>
                                    </div>

                                    <div className="form-group">
                                        <a href="#">
                                            Esqueci minha senha.</a>
                                    </div>

                                    <div className="form-group">
                                        <button className="btn btn-primary btn-block">
                                            ENTRAR
                                        </button>
                                        <button className="btn  btn-block" type="button">
                                            CRIAR CONTA
                                        </button>
                                    </div>
                                    {
                                    handleError(error)
                                } </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </FormUser>
    )
}

export default User
