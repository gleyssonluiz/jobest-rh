import React, { Fragment } from 'react';
import styled from 'styled-components'

const FormUser = styled.nav`
    section {
        height:100vh;
        width:100%;
        display:flex;
        background-color: var(--color-main);
        padding-top: 4em;
    }

    .box{
        display:flex;
        padding:30px 45px;
        background: #FFF;
        box-shadow: 3px 3px 50px var(--color-main-dark);
        min-height:470px;
        border-radius:5px;
    }

    h1{
        text-align:center;
        font-size:22px;
        margin-top:0.5em;
        margin-bottom:2em;
    }

    form{
        flex-basis: 100%;
    }

    form input{
        margin: 5px 0;
    }

    form label{
        margin:0;
        padding:0;
    }

    form button {
        font-weight:700;
        font-size:15px;
    }
   

`
export default FormUser 
