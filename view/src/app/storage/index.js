import config from '../config/'

let prefix =  config.storage.prefix

export default {

    setToken: (token) => {
        localStorage.setItem(`${prefix}token`, token)
    },

    getToken: () => {
        return localStorage.getItem(`${prefix}token`)
    },

    removeToken: () => {
        return localStorage.removeItem(`${prefix}token`)
    }
}